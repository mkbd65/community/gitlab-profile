# Community mods and addons

This group holds mods and addons by the mkbd65 community. You can find custom layouts and other creative ideas around the mkbd65 ecosystem here.

Write an email to mkbd65 (a) xengi (.) de if you want your own repo in this namespace. The default is to mirror your repo into it, so you're not forced into using gitlab if you don't want to.

---

Made with ❤️ and ✨ by you!
